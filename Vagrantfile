# -*- mode: ruby -*-
# vi: set ft=ruby :

# The private network IP of the VM. You will use this IP to connect to OpenShift.
# This variable is ignored for Hyper-V provider.
BOX_NAME = 'cdkv2'

PUBLIC_ADDRESS="10.1.2.2"

#Modify IMAGE_TAG if you need a new OCP version e.g. IMAGE_TAG="v3.3.1.3"
IMAGE_TAG=""

# Number of virtualized CPUs
VM_CPU = ENV['VM_CPU'] || 2

# Amount of available RAM
VM_MEMORY = ENV['VM_MEMORY'] || 3072

# Validate required plugins
REQUIRED_PLUGINS = %w(vagrant-service-manager vagrant-registration vagrant-sshfs vagrant-proxyconf)
errors = []

# Initialize proxy variables
proxy = proxy_user = proxy_password = ''

def message(name)
  "#{name} plugin is not installed, run `vagrant plugin install #{name}` to install it."
end
# Validate and collect error message if plugin is not installed
REQUIRED_PLUGINS.each { |plugin| errors << message(plugin) unless Vagrant.has_plugin?(plugin) }
unless errors.empty?
  msg = errors.size > 1 ? "Errors: \n* #{errors.join("\n* ")}" : "Error: #{errors.first}"
  fail Vagrant::Errors::VagrantError.new, msg
end

file_to_disk = './cdk2_disk/large_disk_cdk.vdi'
Vagrant.configure(2) do |config|
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = "http://wtr-zproprd-hoproxy.johnlewis.co.uk:80"
    config.proxy.https    = "http://wtr-zproprd-hoproxy.johnlewis.co.uk:80"
    config.proxy.no_proxy = "localhost,127.0.0.1,.example.com,.wtr.net,.acceptance.co.uk,.johnlewis.co.uk"
  end
  config.vm.box = if ENV.key?('BOX')
                    ENV['BOX'].empty? ? BOX_NAME : ENV['BOX']
                  else
                    BOX_NAME
                  end
#  config.vm.customize ['createhd', '--filename', file_to_disk, '--size', 500 * 1024]
#  config.vm.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]

  config.vm.provider "virtualbox" do |v, override|
    v.memory = VM_MEMORY
    v.cpus   = VM_CPU
    v.customize ["modifyvm", :id, "--ioapic", "on"]
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    unless File.exist?(file_to_disk)
      v.customize ['createhd', '--filename', file_to_disk, '--size', 500 * 1024]
    end
      v.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
  end

  config.vm.provider "libvirt" do |v, override|
    v.memory = VM_MEMORY
    v.cpus   = VM_CPU
    v.driver = "kvm"
    v.suspend_mode = "managedsave"
  end

  config.vm.provider "hyperv" do |v, override|
    v.memory = VM_MEMORY
    v.cpus   = VM_CPU
  end

  config.vm.network "private_network", ip: "#{PUBLIC_ADDRESS}"
  #config.vm.network "public_network"

  # vagrant-registration
  if ENV.has_key?('SUB_USERNAME') && ENV.has_key?('SUB_PASSWORD')
    config.registration.username = ENV['SUB_USERNAME']
    config.registration.password = ENV['SUB_PASSWORD']
  end

  # Proxy Information from environment
#  if ENV.key?('PROXY')
   # config.registration.proxy = proxy = ENV['PROXY']
   config.registration.proxy = "http://wtr-zproprd-hoproxy.johnlewis.co.uk:80"
  #  config.registration.proxyUser = proxy_user = ENV['PROXY_USER'] if ENV.key?('PROXY_USER')
   # config.registration.proxyPassword = proxy_password = ENV['PROXY_PASSWORD'] if ENV.key?('PROXY_PASSWORD')
 # end

  # vagrant-sshfs
  config.vm.synced_folder '.', '/vagrant', disabled: true
  if Vagrant::Util::Platform.windows?
    target_path = ENV['USERPROFILE'].gsub(/\\/,'/').gsub(/[[:alpha:]]{1}:/){|s|'/' + s.downcase.sub(':', '')}
    config.vm.synced_folder ENV['USERPROFILE'], target_path, type: 'sshfs', sshfs_opts_append: '-o umask=000 -o uid=1000 -o gid=1000'
  else
    config.vm.synced_folder ENV['HOME'], ENV['HOME'], type: 'sshfs', sshfs_opts_append: '-o umask=000 -o uid=1000 -o gid=1000'
  end

  config.vm.provision "shell", inline: <<-SHELL
    sudo setsebool -P virt_sandbox_use_fusefs 1
  SHELL

  # prevent the automatic start of openshift via service-manager by just enabling Docker
  config.servicemanager.services = "docker"

  # explicitly enable and start OpenShift
  config.vm.provision "shell", run: "always", inline: <<-SHELL
    PROXY=#{proxy} PROXY_USER=#{proxy_user} PROXY_PASSWORD=#{proxy_password} IMAGE_TAG=#{IMAGE_TAG} /usr/bin/sccli openshift
  SHELL

  config.vm.provision "shell", run: "always", inline: <<-SHELL
    echo “net.ipv4.ip_forward=1” >> /etc/sysctl.conf
    pvcreate /dev/sdb
    vgextend VolGroup00 /dev/sdb
    lvextend -L 100G /dev/mapper/VolGroup00-docker--pool --resizefs
    echo "DOCKER_STORAGE_OPTIONS=\"--storage-driver devicemapper --storage-opt dm.basesize=20G --storage-opt dm.fs=xfs --storage-opt dm.thinpooldev=/dev/mapper/VolGroup00-docker--pool --storage-opt dm.use_deferred_removal=true \"" > /etc/sysconfig/docker-storage
    echo "INSECURE_REGISTRY='--insecure-registry registry.access.redhat.com'" >> /etc/sysconfig/docker
    systemctl restart network
    systemctl restart docker-storage-setup
    systemctl restart docker 
    subscription-manager repos --enable rhel-7-server-extras-rpms --enable rhel-7-server-optional-rpms --enable rhel-7-server-supplementary-rpms
    yum makecache fast
    #Get the routable IP address of OpenShift
    OS_IP=`/opt/adb/openshift/get_ip_address`
    echo
    echo "Successfully started and provisioned VM with #{VM_CPU} cores and #{VM_MEMORY} MB of memory."
    echo "To modify the number of cores and/or available memory set the environment variables"
    echo "VM_CPU and/or VM_MEMORY respectively."
    echo
    echo "You can now access the OpenShift console on: https://${OS_IP}:8443/console"
    echo
    echo "To download and install OC binary, run:"
    echo "vagrant service-manager install-cli openshift"
    echo
    echo "To use OpenShift CLI, run:"
    echo "$ oc login ${OS_IP}:8443"
    echo
    echo "Configured users are (<username>/<password>):"
    echo "openshift-dev/devel"
    echo "admin/admin"
    echo
    echo "If you have the oc client library on your host, you can also login from your host."
    echo
  SHELL
end
