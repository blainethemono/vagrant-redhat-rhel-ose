# README #


### What is this repository for? ###

* Quick summary

This is a modified version of the Vagrantfile used by the Red Hat Container Development Kit v2.4 (https://developers.redhat.com/products/cdk/overview/) which is located under the rhel-ose directory, created by the CDK 2.4 installer.  

This fixes a number of issues I found, such as ipv4 networking not working, the size of the root filesystem in new containers being 10GB instead of 20G and the thin pool size being far, far too small (I create a thin provisioned 500GB disk and add that to the virtualbox image).

This has only been tested on Mac OSX Sierra but I see no reason why this wouldn't work on the Windows & Linux versions of the CDK.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact